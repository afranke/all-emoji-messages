extern crate regex;

fn main() {
    let should_match = vec!["👍", "😙️", "😙️🤗️", "💪️ 💪", " 😜️", "👨🏼‍💻", "1", "42"];
    let should_not_match = vec!["abc", "Hello! 👋️"];
    let emoji_regex: regex::Regex = regex::Regex::new(r"(?x)
        ^
        [\p{White_Space}\p{Emoji}\p{Emoji_Presentation}\p{Emoji_Modifier}\p{Emoji_Modifier_Base}\p{Emoji_Component}]*
        [\p{Emoji}]+
        [\p{White_Space}\p{Emoji}\p{Emoji_Presentation}\p{Emoji_Modifier}\p{Emoji_Modifier_Base}\p{Emoji_Component}]*
        $
        # That string is made of at least one emoji, possibly more, possibly with modifiers, possibly with spaces, but nothing else
        ").unwrap();

    for message in &should_match {
        assert!(emoji_regex.is_match(message), "Message was: “{}”", message);
    }

    for message in &should_not_match {
        assert!(!emoji_regex.is_match(message), "Message was: “{}”", message);
    }
}
